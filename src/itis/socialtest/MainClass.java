package itis.socialtest;


import itis.socialtest.entities.Post;

import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("D:\\Projects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv", "D:\\Projects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
        MainClass mainClass = new MainClass();
        mainClass.allPosts = new Parsing().makePosts("D:\\Projects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv","D:\\Projects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
        System.out.println(mainClass.analyticsService.findPostsByDate(mainClass.allPosts, "17.04.2021"));
        System.out.println(mainClass.analyticsService.findAllPostsByAuthorNickname(mainClass.allPosts,"varlamov"));
        System.out.println(mainClass.analyticsService.checkPostsThatContainsSearchString(mainClass.allPosts,"Россия"));
        //System.out.println(mainClass.analyticsService.findMostPopularAuthorNickname(mainClass.allPosts));
    }


    private void run(String postsSourcePath, String authorsSourcePath) {
        ArrayList<Post> posts = (ArrayList<Post>) new Parsing().makePosts(postsSourcePath,authorsSourcePath);
        posts.forEach(post -> {
                    System.out.println("Author:" + post.getAuthor().getNickname() + " id: " + post.getAuthor().getId()
                            + " birthday date: " + post.getAuthor().getBirthdayDate());
                    System.out.println("Content:" + post.getContent());
                });
    }
}
