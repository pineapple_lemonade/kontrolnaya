package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Parsing {
	private List<String> parseFile(String path) {
		ArrayList<String> list;
		try {
			File file = new File(path);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			list = (ArrayList<String>) bufferedReader.lines().collect(Collectors.toList());
			return list;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	private List<Author> makeAuthors(String str){
		ArrayList<Author> authors;
		authors = (ArrayList<Author>) parseFile(str)
				.stream()
				.map(s -> {
					String[] split = s.split(",");
					return new Author(Long.parseLong(split[0]),split[1],split[2]);
				})
				.collect(Collectors.toList());
		return authors;
	}
	public List<Post> makePosts(String str, String  str1){
		ArrayList<Post> posts;
		posts = (ArrayList<Post>) parseFile(str)
				.stream()
				.map(s -> {
					String[] split = s.split(",");
					return new Post(split[2],split[3],Long.parseLong(split[1].substring(1)),makeAuthors(str1)
							.stream().filter(author -> author.getId() == Long.parseLong(split[0]))
							.findFirst().orElse(null));
				})
				.collect(Collectors.toList());
		return posts;
	}
}
