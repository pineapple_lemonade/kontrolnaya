package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
	@Override
	public List<Post> findPostsByDate(List<Post> posts, String date) {
		return posts.stream()
				.filter(post -> post.getDate().contains(date))
				.collect(Collectors.toList());
	}

	@Override
	public String findMostPopularAuthorNickname(List<Post> posts) {
//		Map<Long, Author> result = posts.stream()
//				.collect(Collectors.toMap(p -> posts.stream()
//						.filter(q -> q.getAuthor().equals(p.getAuthor()))
//						.mapToLong(Post::getLikesCount)
//						.sum(), Post::getAuthor));
//
//		ArrayList<Long> sortedKeySet = new ArrayList<>();
//		sortedKeySet.addAll(result.keySet());
//		Collections.sort(sortedKeySet);
//		return result.get(sortedKeySet.get(0)).getNickname();
		return null;
	}

	@Override
	public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
		return posts.stream().anyMatch(post -> post.getContent().contains(searchString));
	}

	@Override
	public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
		return posts.stream()
				.filter(post -> post.getAuthor().getNickname().equals(nick))
				.collect(Collectors.toList());
	}
}
